import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  final int days = 30;
  final String name = "Ajay Mistry";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Catalog App"),
      ),
      body: Material(
        child: Center(
          child: Container(
            child: Text("Welcome, $name First Flutter App for $days days"),
          ),
        ),
      ),
      drawer: Drawer(),
    );
  }
}
